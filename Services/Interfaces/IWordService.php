<?php
namespace Base\Services\Interfaces;

interface IWordService extends IBaseService
{
	public function getByWordAndLanguageCode($word, $languageCode);
	public function getByWordAndLanguageId($word, $languageId);
	public function getSuggestionWordDictionary($word, $languageId);
}
