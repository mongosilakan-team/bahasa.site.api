<?php
namespace Base\Services\Interfaces;

interface ITranslationService extends IBaseService
{
	public function checkExistingTranslation($firstWordId, $secondWordId);
}
