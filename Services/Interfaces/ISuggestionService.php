<?php
namespace Base\Services\Interfaces;

interface ISuggestionService
{
	public function saveSuggestion ($entity);
}
