<?php
namespace Base\Services\Interfaces;

interface ILanguageService extends IBaseService
{
    public function getByCode($code);
    public function getActive();
    public function getActiveAndPending();
    public function getUserLanguages();
}
