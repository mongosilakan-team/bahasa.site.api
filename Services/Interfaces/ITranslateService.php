<?php
namespace Base\Services\Interfaces;

interface ITranslateService
{
	public function translate ($from, $to, $source);
	public function getWordDictionaryDetails ($id);
}
