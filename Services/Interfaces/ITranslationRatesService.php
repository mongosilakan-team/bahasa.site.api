<?php
namespace Base\Services\Interfaces;

interface ITranslationRatesService extends IBaseService
{
	// public function checkExistingTranslationRates($translationId, $userId);
	public function saveUserRates($entity);
}
