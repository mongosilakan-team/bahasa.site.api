<?php

namespace Base\Services;

use Base\Framework\Constants;
use Base\Framework\DevTools\BaseDebugger;
use Base\Framework\Library\String;
use Base\Framework\Messages\Message;
use Base\Framework\Plugins\AccessPlugin;
use Base\Framework\Responses\Response;
use Base\Framework\TranslationKit\IndonesianTranslationKit;
use Base\Framework\TranslationKit\NgokoTranslationKit;
use Base\Framework\TranslationKit\TranslationKit;
use Base\Models\Custom\ResultWord;
use Base\Models\Custom\RuleQueue;
use Base\Models\Custom\SourceWord;
use Base\Models\Custom\TranslationResult;
use Base\Repositories\ViewTranslationRepository;
use Base\Resources\Common\CommonResources;
use Base\Resources\Dictionary\DictionaryResources;
use Base\Services\Interfaces\ISuggestionService;

/**
 * Translate service class.
 */
class SuggestionService implements ISuggestionService
{

    private static $_fromLanguage;
    private static $_toLanguage;

    /**
     * Constructor.
     */
    public function __construct()
    {
        // $this->languageService = new LanguageService();
        // $this->wordService = new WordService();
    }

    private static function _saveWordSuggestion($entity, &$response, &$actorWords, $isSourceWord = false){
            $isActive = false;
            if($entity['suggested_by'] == 1){
                $isActive = true;
            }

            $suggestedWord = array();
            $suggestedWord['word'] = $isSourceWord ? $entity['source'] : $entity['result'];
            $suggestedWord['language_id'] = $isSourceWord ? self::$_fromLanguage->id : self::$_toLanguage->id;
            $suggestedWord['status_id'] = $isSourceWord && $entity['source_root_id'] || $isActive ? Constants::Status() ['Active'] : Constants::Status() ['Suggested'];
            $suggestedWord['word_root_id'] = $isSourceWord ? $entity['source_root_id'] : $entity['result_root_id'];
            $suggestedWord['prefix'] = $isSourceWord ? $entity['source_prefix'] : null; // $entity['result_prefix']; // TBD : check result prefix and suffix
            $suggestedWord['suffix'] = $isSourceWord ? $entity['source_suffix'] : null; // $entity['result_suffix'];
            $suggestedWord['suggested_by'] = $entity['suggested_by'];
            $newWord = WordService::create($suggestedWord)->model;
            $word = WordService::getById($newWord->id)->model;
            $actorWords[] = $word;
            return $word;
    }

    public function saveSuggestion($entity)
    {
        // BaseDebugger::debug($entity);die;
        $user = UserService::getById($entity['userSuggestion']['suggested_by'])->model;
        $system = UserService::getById(2)->model;

        $userWords = $systemWords = $userTranslations = $systemTranslations =  $resutlWordTranslations = array();

        $response = new Response();

        self::$_fromLanguage = LanguageService::getByCode($entity['userSuggestion']['source_language_code'])->model;
        // BaseDebugger::debug(self::$_fromLanguage);die;
        self::$_toLanguage = LanguageService::getByCode($entity['userSuggestion']['result_language_code'])->model;

        ### Check word source
        $existedSource = WordService::getByWordAndLanguageId($entity['userSuggestion']['source'], self::$_fromLanguage->id)->model;

        if(!$existedSource){
            $firstWordId = self::_saveWordSuggestion($entity['userSuggestion'], $response, $userWords, true)->id;
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Msg_SuccessfullyAdded'), DictionaryResources::getMessage('Word'), $entity['userSuggestion']['source']));
        }else{
            $firstWordId = $existedSource->id;
        }

        ### Save user suggestion
        // Check word availability
        $existedResult = WordService::getByWordAndLanguageId($entity['userSuggestion']['result'], self::$_toLanguage->id)->model;
        if (!$existedResult) { //save suggested word
            $resutlWordTranslations[] = $secondWord = self::_saveWordSuggestion($entity['userSuggestion'], $response, $userWords);
            $secondWordId = $secondWord->id;
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Msg_SuccessfullyAdded'), DictionaryResources::getMessage('Word'), $entity['userSuggestion']['result']));
        } else { // add existed word to user word
            $resutlWordTranslations[] =  $existedResult;
        }

        //Save system suggestion
        if(isset($entity['systemSuggestions'])){
            foreach ($entity['systemSuggestions'] as $key => $sysSuggWord) {
                $existedResult = WordService::getByWordAndLanguageId($sysSuggWord['result'], self::$_toLanguage->id)->model;
                if(!$existedResult){
                    $resutlWordTranslations[]= self::_saveWordSuggestion($sysSuggWord, $response, $systemWords);
                } else { // add existed word to user word
                    $resutlWordTranslations[] =  $existedResult;
                }
            }
        }

        foreach ($resutlWordTranslations as $key => $word) {
            $translationResult = TranslationService::checkExistingTranslation($firstWordId, $word->id)->model;
            // Save  translation
            if (!$translationResult) {
                // create new translation
                $suggestedTranslation = array();
                $suggestedTranslation['first_word_id'] = $firstWordId;
                $suggestedTranslation['second_word_id'] = $word->id;
                $suggestedTranslation['suggested_by'] = $word->suggested_by;
                $translationResult = TranslationService::create($suggestedTranslation)->model;

                $translation = TranslationService::getById($translationResult->id)->model;
                if($word->suggested_by != 2){
                    $userTranslations[] = $translation;
                } else {
                    $systemTranslations[] = $translation;
                }

                //create translation rates
                $suggestedTranslationRates = array();
                $suggestedTranslationRates['translation_id'] = $translationResult->id;
                $suggestedTranslationRates['suggested_by'] = $word->suggested_by;
                $suggestedTranslationRates['rates'] = 0;
                $translationRatesResult = TranslationRatesService::create($suggestedTranslationRates)->model;

                $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(DictionaryResources::getMessage('Msg_TranslationSuccessfullyAdded'), $entity['userSuggestion']['source'], $entity['userSuggestion']['result']));
            }
        }

        $user->words = $userWords;
        $user->translations = $userTranslations;
        $user->save();

        $system->words = $systemWords;
        $system->translations = $systemTranslations;
        $system->save();

        //Get the updated translations
        $translateService = new TranslateService();
        $translateResult = $translateService->translate(self::$_fromLanguage->code, self::$_toLanguage->code, $entity['userSuggestion']['source'])->model;
        $response->model = $translateResult->advanced;
        $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Thanks'), $user->name));

        return $response;
    }
}
