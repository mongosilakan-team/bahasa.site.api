<?php

namespace Base\Services;

use Base\Framework\Constants;
use Base\Framework\DevTools\BaseDebugger;
use Base\Framework\Library\String;
use Base\Framework\Messages\Message;
use Base\Framework\Plugins\AccessPlugin;
use Base\Framework\Responses\Response;
use Base\Framework\TranslationKit\IndonesianTranslationKit;
use Base\Framework\TranslationKit\NgokoTranslationKit;
use Base\Framework\TranslationKit\TranslationKit;
use Base\Models\Custom\ResultWord;
use Base\Models\Custom\RuleQueue;
use Base\Models\Custom\SourceWord;
use Base\Models\Custom\TranslationResult;
use Base\Repositories\ViewTranslationRepository;
use Base\Repositories\SPViewTranslationRepository;
use Base\Resources\Common\CommonResources;
use Base\Resources\Dictionary\DictionaryResources;
use Base\Services\Interfaces\ITranslateService;
use Base\Models\Custom\ViewTranslationResponse;

/**
 * Translate service class.
 */
class TranslateService implements ITranslateService
{
    /**
     * Service of language.
     *
     * @var class
     */
    private $languageService = null;

    private $wordService = null;

    // Define private variable used in this class

    /**
     * From language identifier.
     *
     * @var int
     */
    private $fromLanguage;

    /**
     * To language identifier.
     *
     * @var int
     */
    private $toLanguage;

    /**
     * combinatino of word eg. air mata.
     *
     * @var string
     */
    private $combinationWord = '';

    /**
     * Result bulk.
     *
     * @var string
     */
    private $resultBulk;

    /**
     * Is combination possible eg. air in air mata.
     *
     * @var bool
     */
    private $isCombinationPossible = false;

    /**
     * Count of combination word eg. air mata -> 2.
     *
     * @var int
     */
    private $combinationWordsCount = 0;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->languageService = new LanguageService();
        $this->wordService = new WordService();
    }

    public function getWordDictionaryDetails($id)
    {
        $response = new Response();
        $result = ViewTranslationRepository::getSingleWordByWordId($id);
        $response->model = $result;

        return $response;
    }

    public function translate($from, $to, $source)
    {
        $this->fromLanguage = $this->languageService->getByCode($from)->model;

        $this->toLanguage = $this->languageService->getByCode($to)->model;

        $sourceWords = $this->_convertSourceToObject($source, $this->fromLanguage->id);

        $result = $combination = $ruleQueue = $this->resultBulk = array();

        $savedRule = null;

        ##GET bulk
        // $sourceBulk = str_replace(',,', ',', implode(',', array_map(function ($el) { return $el->isAlphabetic ? '"'.$el->word.'"' : null; }, $sourceWords)));
        // BaseDebugger::debug($sourceBulk);die;
        $sourceBulk = preg_replace('/[^A-Za-z0-9 ]/', ' ', $source);
        $sourceBulk = preg_replace('/\s+/', ' ', $sourceBulk);

        $sourceBulk = trim($sourceBulk);
        $sourceBulk = implode(' ', array_unique(explode(' ', $sourceBulk)));

        if($this->fromLanguage->id == 1 or $this->toLanguage->id == 1){
            // BaseDebugger::debug("etset");die;
            $sourceBulk = '"'.str_replace(' ', '", "', $sourceBulk ).'"';
            $entities = ViewTranslationRepository::getBulkWords($sourceBulk, $this->fromLanguage->id, $this->toLanguage->id);
            foreach ($entities->member as $type) {
                $this->resultBulk[$type->source][] = $type;
            }
        }else{
            $sourceBulk = str_replace(' ', ',', $sourceBulk);
            $entities = SPViewTranslationRepository::getBulkWords($sourceBulk, $this->fromLanguage->id, $this->toLanguage->id);
            foreach ($entities->member as $entity) {
                $this->resultBulk[$entity->source][] = $entity;
            }
            // BaseDebugger::debug($this->resultBulk);die;
        }


        //  BaseDebugger::debug($this->resultBulk);die;

        //  BaseDebugger::debug($this->resultBulk['aku']);
        //  die;
        // BaseDebugger::debug($this->resultBulk);die;
        // $entities = ViewTranslationRepository::getBulkWords($sourceBulk, $this->fromLanguage->id, $this->toLanguage->id);
        // BaseDebugger::debug($entities);die;
        // $this->resultBulk = array();
        // foreach ($entities->member as $type) {
        //     $this->resultBulk[$type->source][] = $type;
        // }
        // die;
        ###

        ## Optimization : Still can be improved by get bulk source
        foreach ($sourceWords as $key => $sourceWord) {
            $resultWords = array();

            ## Non Alphabetic characters
            if (!$sourceWord->isAlphabetic) {
                ## combination checker (cc)
                $nonAlpha = ResultWord::base($sourceWord->word, false, $sourceWord->order, $sourceWord->position);
                if (count($combination)) {
                    $combination[] = $nonAlpha;
                }
                ## end (cc)

                $resultWords[] = $nonAlpha;
                $result[] = $resultWords;
                continue;
            }

            ## Alphabetic characters
            if ($this->_getSingleWord($sourceWord, $resultWords)) {
                // BaseDebugger::debug($resultWords);
                ##  combination checker (cc)
                $combination[] = $resultWords[0];
                $this->_checkCombination($combination, $resultWords, $result);
                // BaseDebugger::debug($resultWords);die;
                ## end (cc)

                ## rule checker (rc)
                if($resultWords[0]->numberOfRules){
                    $this->_checkWordRule($ruleQueue, $resultWords[0], $result);
                }
                # end (rc)
            } else {
                $resultWords[] = ResultWord::base($sourceWord->word, true, $sourceWord->order, $sourceWord->position);
            }

            foreach ($ruleQueue as $key => $rule) {
                if (!$rule->counter) {
                    $this->_applyRule($rule->id, $result, $resultWords);
                }
                --$rule->counter;
            }
            $result[] = $resultWords;
            ## sort by order
            usort($result, function ($a, $b) {
                return $a[0]->order - $b[0]->order;
            });
        }

        $translationResult = new TranslationResult();

        foreach ($result as $key => $value) {
            $translationResult->basic .= $value[0]->result;
            $translationResult->advanced[] = $value;
        }
        // BaseDebugger::debug($translationResult);
        // die;
        $response = new Response();

        $response->model = $translationResult;
        // die;
        return $response;
    }

    private function _checkWordRule(&$ruleQueue, $resultWord, $result)
    {
        $word = $this->wordService->getById($resultWord->sourceId, array('cache' => array('key' => 'word')))->model;
        $wordRules = $word->getRules();
        $wordRuleId = $savedRule = !isset($wordRules[0]) ? null : $wordRules[0]->id;
        switch ($wordRuleId) {
            case Constants::Rule('SwitchNext'):
                $ruleQueue[] = RuleQueue::init($wordRuleId, 1);
                break;
            case Constants::Rule('SwitchPrevious'):
                if(count($result) < 1 || $resultWord->isHasParticle || $result[count($result) - 2][0]->wordTypeId != Constants::WordTypes('Number')) { // dikali tiga
                    return;
                }
                $ruleQueue[] = RuleQueue::init($wordRuleId);
                break;
            default:
                break;
        }
    }

    private function _applyRule($id, &$result, &$resultWords)
    {
        switch ($id) {
            case Constants::Rule('SwitchNext'):
                $this->_swapOrder($result[count($result) - 2], $resultWords);
                break;
            case Constants::Rule('SwitchPrevious'):
                $this->_swapOrder($result[count($result) - 2], $resultWords);
                break;
            default:
                # code...
                break;
        }
    }

    private function _swapOrder(&$swap_a, &$swap_b)
    {
        $temp = $swap_a[0]->order;
        foreach ($swap_a as $key => $item) {
            $item->order = $swap_b[0]->order;
        }

        foreach ($swap_b as $key => $item) {
            $item->order = $temp;
        }
    }

    private function _checkCombination(&$combination, &$resultWords, &$result)
    {
        $sourceWord = $resultWord = $orders = array();
        $word = '';
        foreach ($combination as $key => $item) {
            $word .= $item->source;
            $orders[] = $item->order;
        }
        // BaseDebugger::debug($word);
        $sourceWord = new SourceWord($word, $this->fromLanguage->id, strlen($word), array($combination[0]->sourcePosition[0], $combination[count($combination) - 1]->sourcePosition[1]), true, $combination[0]->order);

        // BaseDebugger::debug(end($resultWords));
        // if (str_word_count($sourceWord->word) > 1 && $this->_getSingleWord($sourceWord, $resultWord, true)) {
        if (str_word_count($sourceWord->word) > 1 && $this->_getSingleWord($sourceWord, $resultWord, true)) {
            // BaseDebugger::debug($result);
            $default = !$resultWord ? null : $resultWord[0];
            if (!$default->possibleCombination) {
                foreach ($result as $key => $item) {
                    $defaultResult = $item[0];
                    if (in_array($defaultResult->order, $orders)) {
                        unset($result[$key]);
                    }
                }
                $result = array_values($result);
                $combination = array();
            }

            $resultWords = $resultWord;
        }
        elseif(!end($resultWords)->possibleCombination){
            $combination = array();
        }
    }

    private function _convertSourceToObject($source, $from)
    {
        $sourceWords = array();

        $startLen = 0;

        foreach (preg_split('/([A-Za-z]+)/', $source, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY) as  $keyItem => $item) {
            $length = strlen($item);

            $sourceWords[] = new SourceWord($item, $from, $length, array($startLen, $startLen + $length), ctype_alpha($item), $keyItem);

            $startLen += $length;
        }

        return $sourceWords;
    }

    private function _getSingleWord($sourceWord, &$resultWords, $isCheckOnDb = false)
    {
        // BaseDebugger::debug($isCheckOnDb);
        // Check the original word
        if ($this->_getTranslation($sourceWord, $resultWords, $isCheckOnDb)) {
            return true;
        }

        if ($this->_getTranslationWithPrefix($sourceWord, $resultWords)) {
            return true;
        }

        if ($this->_getTranslationWithSuffix($sourceWord, $resultWords)) {
            return true;
        }

        if ($this->_getTranslationWithPrefixAndSuffix($sourceWord, $resultWords)) {
            return true;
        }

        return false;
    }

    // private function _getPossibleCombination($sourceWord, &$resultWords)
    // {
    //     $entities = ViewTranslationRepository::checkCombinationOfWord($sourceWord->word, $sourceWord->languageId, $this->toLanguage->id);
    //     // BaseDebugger::debug($entities);die;
    //     if ($this->_resultMapper($entities, $sourceWord, $resultWords)) {
    //         return true;
    //     }
    //
    //     return false;
    // }

    private function _getTranslation($sourceWord, &$resultWords, $isCheckOnDb)
    {
        if ($isCheckOnDb) {
            // BaseDebugger::debug($isCheckOnDb);die;
            $entities = ViewTranslationRepository::getSingleWord($sourceWord->word, $sourceWord->languageId, $this->toLanguage->id);
                // BaseDebugger::debug($sourceWord->word);die;
        } else {
            // BaseDebugger::debug($isCheckOnDb);die;
            $entities =  new ViewTranslationResponse();//(object) array();
            $entities->isModified = false;
            if (isset($this->resultBulk[$sourceWord->word])) {
                $entities->member = $this->resultBulk[$sourceWord->word];
                // bellow to be reviewed to delete
                $entities->isHasParticle = $entities->member[0]->is_source_has_particle;
                $entities->sourcePrefix = $entities->member[0]->source_prefix;
                $entities->sourceSuffix = $entities->member[0]->source_suffix;
                // end tbr
            }
        }
        // $entities = ViewTranslationRepository::getSingleWord($sourceWord->word, $sourceWord->languageId, $this->toLanguage->id);
        // if (count($entities->member)) {
        //     $rootEntity = $entities->member[0];
        //     $rootEntities = ViewTranslationRepository::getSingleWordByWordId($rootEntity->source_root_id, $this->toLanguage->id);
        //     if($rootEntity->source_prefix){
        //         $prefixType = TranslationKit::GetParticleType($rootEntity->source_prefix, Constants::Prefix($this->fromLanguage->code));
        //         $rootEntities->prefixType = $prefixType;
        //         self::_adjustPrefix($rootEntities, $this->toLanguage->code);
        //     }
        //     BaseDebugger::debug($rootEntities);die;
        // }
        // BaseDebugger::debug($entities);die;

        if ($this->_resultMapper($entities, $sourceWord, $resultWords)) {
            return true;
        }

        return false;
    }

    private function _resultMapper($entities, $sourceWord, &$resultWords)
    {
        if (isset($entities->member) && count($entities->member)) {
            foreach ($entities->member as $member) {
                $resultWords[] = ResultWord::found(
                    $sourceWord->word,
                    $entities->isModified ? null : $member->source_id,
                    $member->translation_id,
                    $member->result,
                    $entities->isModified ? null : $member->result_id,
                    $member->result_status_id,
                    $member->is_on_dictionary,
                    $member->point,
                    $sourceWord->isAlphabetic,
                    $sourceWord->order,
                    $sourceWord->position,
                    $member->possible_combination,
                    $member->word_type_id,
                    $entities->isHasParticle,
                    $member->rates,
                    $member->number_of_rules,
                    $entities->isHasParticle ? $member->source_id : $member->source_root_id,
                    $entities->sourcePrefix,
                    $entities->sourceSuffix,
                    $entities->isHasParticle ? $member->result_id : $member->result_root_id
                );
            }

            return true;
        }

        return false;
    }

    private function _getTranslationWithPrefix($sourceWord, &$resultWords)
    {
        $entities = (object) array();
        $entities->member = array(); // the result is modification from system. not pure from database

        foreach (Constants::Prefix($this->fromLanguage->code) as $keyType => $prefixType) {
            foreach ($prefixType as $key => $prefix) {
                $prefixToCheck = substr($sourceWord->word, 0, strlen($prefix));

                $sourceWord->root = substr($sourceWord->word, strlen($prefix));

                if ($prefix != $prefixToCheck || !$sourceWord->root) {
                    continue;
                }

                $sourceWord->prefix = $prefix;

                $sourceWord->prefixType = $keyType;

                switch ($this->fromLanguage->code) {
                    case 'id-ID':
                        $entities = IndonesianTranslationKit::getTranslation($sourceWord, $this->fromLanguage->id, $this->toLanguage->id);
                        break;

                    case 'jv-NG':
                        break;

                    default:
                        break;
                }

                if (count($entities->member)) {
                    self::_adjustPrefix($entities, $this->toLanguage->code);
                }
            }

            if (count($entities->member)) {
                break;
            }
        }

        // BaseDebugger::debug($resultWords);

        if ($this->_resultMapper($entities, $sourceWord, $resultWords)) {
            return true;
        }

        return false;
    }

    private function _getTranslationWithSuffix($sourceWord, &$resultWords, &$entities = null)
    {
        $isCheckOnly = $entities ? true : false;

        $entities = (object) array();
        $entities->member = array();

        foreach (Constants::Suffix($this->fromLanguage->code) as $keyType => $suffixType) {
            foreach ($suffixType as $key => $suffix) {
                $suffixToCheck = substr($sourceWord->word, -strlen($suffix));

                $sourceWord->root = substr($sourceWord->word, 0, -strlen($suffix));

                if ($suffix != $suffixToCheck || !$sourceWord->root) {
                    continue;
                }

                $sourceWord->suffix = $suffix;

                $sourceWord->suffixType = $keyType;

                switch ($this->fromLanguage->code) {
                    case 'id-ID':
                        $entities = IndonesianTranslationKit::getTranslation($sourceWord, $this->fromLanguage->id, $this->toLanguage->id);
                        break;

                    case 'jv-NG':

                        break;

                    default:
                        break;
              }

                if (count($entities->member)) {
                    self::_adjustSuffix($entities, $this->toLanguage->code);
                }
            }

            if (count($entities->member)) {
                break;
            }
        }

        if ($isCheckOnly) {
            return;
        }

        if ($this->_resultMapper($entities, $sourceWord, $resultWords)) {
            return true;
        }

        return false;
    }

    private function _getTranslationWithPrefixAndSuffix($sourceWord, &$resultWords)
    {
        $entities = (object) array();
        $entities->member = array();

        $original = $sourceWord->word;

        //check prefix
        foreach (Constants::Prefix($this->fromLanguage->code) as $prefixKeyType => $prefixType) {
            foreach ($prefixType as $key => $prefix) {
                $prefixToCheck = substr($original, 0, strlen($prefix));
                // eg. dibawakan -> bawakan
                $sourceWord->word = substr($original, strlen($prefix));

                if ($prefix != $prefixToCheck || !$sourceWord->root) {
                    continue;
                }

                $sourceWord->prefix = $prefix;

                $sourceWord->prefixType = $prefixKeyType;

                $this->_getTranslationWithSuffix($sourceWord, $resultWords, $entities);

                if (count($entities->member)) {
                    self::_adjustPrefix($entities, $this->toLanguage->code);
                }
            }
        }

        $sourceWord->word = $original;

        if ($this->_resultMapper($entities, $sourceWord, $resultWords)) {
            return true;
        }

        return false;
    }

    /**
     * Adjust prefix after word found.
     *
     * @param object $entities collection of word result
     * @param string $language code
     *
     * @return object $entities
     */
    private static function _adjustPrefix(&$entities, $language)
    {
        switch ($language) {
          case 'id-ID':
              // code...
              break;

          case 'jv-NG':
              NgokoTranslationKit::adjustPrefix($entities);
              break;
          default:
              // code...
              break;
        }
    }

    /**
     * Adjust suffix after word found.
     *
     * @param object $entities collection of word result
     * @param string $language code
     *
     * @return object $entities
     */
    private static function _adjustSuffix(&$entities, $language)
    {
        switch ($language) {
          case 'id-ID':
              // code...
              break;

          case 'jv-NG':
              NgokoTranslationKit::adjustSuffix($entities);
              break;
          default:
              // code...
              break;
        }
    }
}
