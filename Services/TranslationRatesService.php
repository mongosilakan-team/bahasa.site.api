<?php
namespace Base\Services;
use Base\Framework\Constants;
use Base\Framework\Library\String;
use Base\Framework\Messages\Message;
use Base\Framework\Responses\Response;
use Base\Repositories\TranslationRatesRepository;
use Base\Resources\Common\CommonResources;
use Base\Services\Interfaces\ITranslationRatesService;
use Base\Framework\Exceptions\CustomException;
use Base\Framework\DevTools\BaseDebugger;
use Base\Framework\Plugins\AccessPlugin;

/**
* TranslationRates service
*/
class TranslationRatesService implements ITranslationRatesService
{
    /**
    * @return the collection of related entity
    */
    public function getAll() {
        $response = new Response();
        $response->model = TranslationRatesRepository::getAll();
        return $response;
    }
    /**
    * @param  criteria
    * @return the collection of related entity
    */
    public function search($criteria) {
        $response = new Response();
        $repository = new TranslationRatesRepository();
        $result = $repository->search($criteria);
        $response->model = $result;
        return $response;
    }

    /**
    * @param  identifier
    * @return the related entity
    */
    public function getById($id) {
        $response = new Response();
        $result = TranslationRatesRepository::getById($id);

        if ($result) {
            $response->model = $result;
        }
        else {
            $response->messages[] = new Message(null, Constants::getMessageType() ['Error'], String::format(CommonResources::getMessage('NotFound') , CommonResources::getMessage('TranslationRates')));
            throw new CustomException($response->messages, Constants::errorCode() ['NotFound']);
        }
        return $response;
    }
    //
    // public function checkExistingTranslationRates($translationId, $userId){
    //     $response = new Response();
    //     $result = TranslationRatesRepository::checkExistingTranslationRates($translationId, $userId);
    //
    //     if ($result) {
    //         $response->model = $result;
    //     }
    //     else {
    //         $response->messages[] = new Message(null, Constants::getMessageType() ['Error'], String::format(CommonResources::getMessage('NotFound') , DictionaryResources::getMessage('TranslationRates')));
    //     }
    //     return $response;
    // }

    public function saveUserRates($entity)
    {
        $user = AccessPlugin::getCurrentUser();
        $response = new Response();

        $userRates = TranslationRatesRepository::checkExistingTranslationRates($entity['first_word_id'], $entity['second_word_id'], $entity['suggested_by']);
        // BaseDebugger::debug($userRates);die;
        if(!$userRates){
            // BaseDebugger::debug($userRates);die;
            $userRateResult = TranslationRatesRepository::create($entity);
        } else{
            $entity['id'] = $userRates->id;
            $entity['created_at'] = $userRates->created_at;
            $userRateResult = TranslationRatesRepository::update($entity);
        }
        $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Thanks'), $user['name']));
        return $response;
    }

    /**
    * @param  entity
    * @return the messages
    */
    public function create($entity){
        $response = new Response();
        $entity['skip_attributes'] = array();
        $model = TranslationRatesRepository::create($entity);

        if($model->getMessages()){
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);

        }else{
            $response->model = $model;
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Msg_SuccessfullyCreated') , CommonResources::getMessage('TranslationRates') , $entity['translation_id']));
        }

        return $response;
    }
    /**
    * @param  entity
    * @return the messages
    */
    public function update($entity){
        $response = new Response();
        $entity['skip_attributes'] = array('created_at');
        $model = TranslationRatesRepository::update($entity);
        if($model->getMessages()){
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);
        }else{
            $response->model = $model;
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Msg_SuccessfullyUpdated') , CommonResources::getMessage('TranslationRates') , $entity['translation_id']));
        }
        return $response;
    }
    /**
    * @param  identfier
    * @return the messages
    */
    public function delete($id) {
        $response = new Response();
        $entity = TranslationRatesRepository::getById($id);
        $model = TranslationRatesRepository::delete($id);
        if($model->getMessages()){
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);
        }else{
            $response->model = $model;
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Msg_SuccessfullyDeleted') , CommonResources::getMessage('TranslationRates') , $entity->translation_id));
        }

        return $response;
    }
}
