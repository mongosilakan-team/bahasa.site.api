<?php
namespace Base\Services;
use Base\Framework\Constants;
use Base\Framework\Library\String;
use Base\Framework\Messages\Message;
use Base\Framework\Responses\Response;
use Base\Repositories\ParticleRepository;
use Base\Resources\Common\CommonResources;
use Base\Resources\Dictionary\DictionaryResources;
use Base\Services\Interfaces\IParticleService;
use Base\Framework\Exceptions\CustomException;

/**
* Particle service
*/
class ParticleService implements IParticleService
{
    public function __construct() {
    }

    /**
    * @return the collection of language
    */
    public function getAll() {
        $response = new Response();
        $response->model = ParticleRepository::getAll();
        return $response;
    }
    /**
    * @param  criteria
    * @return the collection of language
    */
    public function search($criteria) {
        $response = new Response();
        $repository = new ParticleRepository();
        $result = $repository->search($criteria);
        $response->model = $result;
        return $response;
    }

    /**
    * @param  identifier
    * @return the language
    */
    public function getById($id) {
        $response = new Response();
        $result = ParticleRepository::getById($id);

        if ($result) {
            $response->model = $result;
        }
        else {
            $response->messages[] = new Message(null, Constants::getMessageType() ['Error'], String::format(CommonResources::getMessage('NotFound') , DictionaryResources::getMessage('Particle')));
            throw new CustomException($response->messages, Constants::errorCode() ['NotFound']);
        }
        return $response;
    }

    /**
     * [getByTypeAndLanguage description]
     * @param  [type] $type     [description]
     * @param  [type] $language_id [description]
     * @return [type]           [description]
     */
    public function getByTypeAndLanguageId($type, $language_id){
        $response = new Response();
        $result = ParticleRepository::getByTypeAndLanguageId($type, $language_id);

        if ($result) {
            $response->model = $result;
        }
        else {
            $response->messages[] = new Message(null, Constants::getMessageType() ['Error'], String::format(CommonResources::getMessage('NotFound') , DictionaryResources::getMessage('Particle')));
            throw new CustomException($response->messages, Constants::errorCode() ['NotFound']);
        }
        return $response;
    }

    /**
    * @param  entity
    * @return the messages
    */
    public function create($entity){
        $response = new Response();
        $entity['skip_attributes'] = array();
        $model = ParticleRepository::create($entity);

        if($model->getMessages()){
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);

        }else{
            $response->model = $model;
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Msg_SuccessfullyCreated') , DictionaryResources::getMessage('Particle') , $entity['name']));
        }

        return $response;
    }
    /**
    * @param  entity
    * @return the messages
    */
    public function update($entity){
        $response = new Response();
        $entity['skip_attributes'] = array('created_at');
        $errMsg = ParticleRepository::update($entity);
        if($errMsg){
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);
        }else{
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Msg_SuccessfullyUpdated') , DictionaryResources::getMessage('Particle') , $entity['name']));
        }
        return $response;
    }
    /**
    * @param  identfier
    * @return the messages
    */
    public function delete($id) {
        $response = new Response();
        $entity = ParticleRepository::getById($id);
        $errMsg = ParticleRepository::delete($id);
        if($errMsg){
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);
        }else{
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], String::format(CommonResources::getMessage('Msg_SuccessfullyDeleted') , DictionaryResources::getMessage('Particle') , $entity->name));
        }

        return $response;
    }
}
