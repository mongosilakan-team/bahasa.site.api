<?php

namespace Base\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class RolePrivileges extends \Phalcon\Mvc\Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $role_id;

    /**
     * @var int
     */
    public $privilege_id;

    /**
     * @var string
     */
    public $created_at;

    public function initialize()
    {
        $this->belongsTo('role_id', "Base\Models\Role", 'id');
        $this->belongsTo('privilege_id', "Base\Models\Privilege", 'id');
    }

    public function getSource()
    {
        return 'role_privileges';
    }

    /**
     * Allows to query a set of records that match the specified conditions.
     *
     * @param mixed $parameters
     *
     * @return RobotsParts[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions.
     *
     * @param mixed $parameters
     *
     * @return RobotsParts
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

	public function validation()
    {
        $this->validate(
            new UniquenessValidator(
                array(
                    "field"   => array('privilege_id', 'role_id'),
                    "message" => "Role privileges must be unique."
                )
            )
        );

        return $this->validationHasFailed() != true;
    }
}
