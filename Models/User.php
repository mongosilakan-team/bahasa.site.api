<?php

namespace Base\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;
use Base\Framework\Messages\Message;
use Base\Framework\Constants;
use Base\Framework\Library\String;
use Base\Resources\Common\CommonResources;
use Base\Resources\Configuration\ConfigurationResources;

/**
 * User class.
 */
class User extends BaseModel
{
    /**
     * username.
     *
     * @var string
     */
    public $username;

    /**
     * password.
     *
     * @var string
     */
    public $password;

    /**
     * name.
     *
     * @var string
     */
    public $name;

    /**
     * email.
     *
     * @var string
     */
    public $email;

    /**
     * status id.
     *
     * @var int
     */
    public $status_id;

    /**
     * Language identifier.
     *
     * @var int
     */
    public $language_id;

    public $google_id;

    public $email_verified;

    public $picture;

    public $locale;

    /**
     * This model is mapped to the table users.
     */
    public function getSource()
    {
        return 'users';
    }

    /**
     * A car only has a Brand, but a Brand have many Cars.
     */
    public function initialize()
    {
        $this->belongsTo('status_id', 'Base\Models\Status', 'id');
        $this->belongsTo('language_id', 'Base\Models\Language', 'id');
        $this->hasManyToMany(
            'id',
            "Base\Models\UserRoles",
            'user_id', 'role_id',
            "Base\Models\Role",
            'id', array('alias' => 'roles')
        );
        $this->hasManyToMany(
            'id',
            "Base\Models\UserTranslations",
            'user_id', 'translation_id',
            "Base\Models\Translation",
            'id', array('alias' => 'translations')
        );
        $this->hasManyToMany(
            'id',
            "Base\Models\UserWords",
            'user_id', 'word_id',
            "Base\Models\Word",
            'id', array('alias' => 'words')
        );
        $this->hasManyToMany(
            'id',
            "Base\Models\UserTrustedLanguages",
            'user_id', 'language_id',
            "Base\Models\Language",
            'id', array('alias' => 'trustedLanguages')
        );
    }

    public function getRoles($parameters = null)
    {
        return  $this->getRelated('Base\Models\Role', $parameters);
    }

    /**
     * @return bool
     */
    public function validation()
    {
        $this->validate(new UniquenessValidator(array('field' => 'username', 'message' => String::format(CommonResources::getMessage('Msg_Uniqueness'), ConfigurationResources::getMessage('username'), $this->username))));
        $this->validate(new EmailValidator(array('field' => 'email', 'message' => String::format(CommonResources::getMessage('Msg_InvalidEmail'), ConfigurationResources::getMessage('email')))));
        $this->validate(new UniquenessValidator(array('field' => 'email', 'message' => String::format(CommonResources::getMessage('Msg_Uniqueness'), ConfigurationResources::getMessage('email'), $this->email))));
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    public function getMessages()
    {
        $messages = array();
        foreach (parent::getMessages() as $message) {
            switch ($message->getType()) {

                    // case 'InvalidCreateAttempt':
                    //     $messages[] = new Message(null, Constants::getMessageType() ['Error'], String::format(CommonResources::getMessage('Msg_InvalidCreateAttempt') , CommonResources::getMessage('User')));
                    //     break;
                    // case 'InvalidUpdateAttempt':
                    //     $messages[] = new Message(null, Constants::getMessageType() ['Error'], String::format(CommonResources::getMessage('Msg_InvalidUpdateAttempt') , CommonResources::getMessage('User')));
                    //     break;

                case 'PresenceOf':
                    $messages[] = new Message(null, Constants::getMessageType() ['Error'], String::format(CommonResources::getMessage('Msg_PresenceOf'), ConfigurationResources::getMessage($message->getField())));
                    break;

                default:
                    $messages[] = new Message(null, Constants::getMessageType() ['Error'], $message->getMessage());
                    break;
            }
        }

        return $messages;
    }
}
