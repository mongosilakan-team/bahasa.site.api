<?php

namespace Base\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class UserTranslations extends \Phalcon\Mvc\Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var int
     */
    public $translation_id;

    /**
     * @var string
     */
    // public $created_at;

    public function initialize()
    {
        $this->belongsTo('user_id', "Base\Models\User", 'id');
        $this->belongsTo('translation_id', "Base\Models\Translation", 'id');
    }

    public function getSource() {
        return 'user_translations';
    }

	public function beforeValidationOnCreate() {
        $this->created_at = gmdate('Y-m-d H:i:s');
    }

	public function validation()
    {
        $this->validate(
            new UniquenessValidator(
                array(
                    "field"   => array('user_id', 'translation_id'),
                    "message" => "User translations must be unique."
                )
            )
        );

        return $this->validationHasFailed() != true;
    }
}
