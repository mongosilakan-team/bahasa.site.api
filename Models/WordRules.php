<?php

namespace Base\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class WordRules extends CacheableBaseModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $word_id;

    /**
     * @var int
     */
    public $rule_id;

    /**
     * @var string
     */
    public $created_at;

    public function initialize()
    {
        $this->belongsTo('word_id', "Base\Models\Word", 'id',
            array(
                'reusable' => true,
            ));
        $this->belongsTo('rule_id', "Base\Models\Rule", 'id',
            array(
                'reusable' => true,
            ));
    }

    public function getSource()
    {
        return 'word_rules';
    }

    /**
     * Allows to query a set of records that match the specified conditions.
     *
     * @param mixed $parameters
     *
     * @return RobotsParts[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions.
     *
     * @param mixed $parameters
     *
     * @return RobotsParts
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function validation()
    {
        $this->validate(
            new UniquenessValidator(
                array(
                    'field' => array('word_id', 'rule_id'),
                    'message' => 'Word rule must be unique.',
                )
            )
        );

        return $this->validationHasFailed() != true;
    }
}
