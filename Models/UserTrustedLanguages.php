<?php

namespace Base\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class UserTrustedLanguages extends \Phalcon\Mvc\Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var int
     */
    public $language_id;

    /**
     * @var string
     */
    // public $created_at;

    public function initialize()
    {
        $this->belongsTo('user_id', "Base\Models\User", 'id');
        $this->belongsTo('language_id', "Base\Models\Language", 'id');
    }

    public function getSource()
    {
        return 'user_trusted_languages';
    }

    public function beforeValidationOnCreate()
    {
        $this->created_at = gmdate('Y-m-d H:i:s');
    }

    public function validation()
    {
        $this->validate(
            new UniquenessValidator(
                array(
                    'field' => array('user_id', 'language_id'),
                    'message' => 'User words must be unique.',
                )
            )
        );

        return $this->validationHasFailed() != true;
    }
}
