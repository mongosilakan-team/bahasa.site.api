<?php
namespace Base\Models;

use Phalcon\Mvc\Model;

/**
 * ManageTranslation class
 */
class BaseTranslation extends Model
{
    public function getSource() {
        return 'vw_base_translation';
    }
    
}
