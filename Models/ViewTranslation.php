<?php
namespace Base\Models;

use Phalcon\Mvc\Model;

/**
 * ViewTranslation class
 */
class ViewTranslation extends Model
{
    public $source;

    public $source_id;

    public $translation_id;

    public $result_id;

    public $result;

    public $result_language_id;

    public $result_status_id;

    public $is_on_dictionary;

    public $word_type_id;

    public $point;

    public function getSource() {
        return 'ViewTranslation';
    }

    public function initialize() {
        // $this->belongsTo('result_language_id', 'Base\Models\Status', 'id');
        // $this->belongsTo('result_status_id', 'Base\Models\Status', 'id');
        // $this->belongsTo('suggested_by', 'Base\Models\User', 'id');
    }

    public function getPoint() {
        return (int)$this->point;
    }

    public function afterFetch()
    {
        //$this->status = explode(',', $this->status);
    }

}
