<?php
namespace Base\Models;

class Privilege extends BaseModel
{

    /**
     *
     * @var string
     */
    public $route;

	public $description;

	public $status_id;

    public function initialize()
   {
       $this->belongsTo('status_id', 'Base\Models\Status', 'id');
       $this->hasManyToMany(
           "id",
           "Base\Models\RolePrivileges",
           "privilege_id", "role_id",
           "Base\Models\Role",
           "id"
       );
   }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'privileges';
    }



    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Parts[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Parts
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
