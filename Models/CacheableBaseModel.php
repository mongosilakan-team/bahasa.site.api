<?php

namespace Base\Models;

use Phalcon\Mvc\Model;

/**
 * BaseModel class.
 */
class CacheableBaseModel extends Model
{
    /**
     * identifier.
     *
     * @var int
     */
    public $id;

    /**
     * created at.
     *
     * @var datetime
     */
    public $created_at;

    /**
     * modified.
     *
     * @var datetime
     */
    public $modified_at;

    /**
     * Additional attribute.
     */
    public $skip_attributes = array();

    public function beforeValidationOnCreate()
    {
        $this->created_at = gmdate('Y-m-d H:i:s');
        $this->modified_at = gmdate('Y-m-d H:i:s');
    }

    public function beforeValidationOnUpdate()
    {
        $this->modified_at = gmdate('Y-m-d H:i:s');
    }

    public function beforeUpdate()
    {
        $this->modified_at = gmdate('Y-m-d H:i:s');
        $this->skipAttributesOnUpdate($this->skip_attributes);
    }

    protected static $_cache = array();

    protected static function _createKey($parameters)
    {
        $uniqueKey = array();

        foreach ($parameters as $key => $value) {
            if (is_scalar($value)) {
                $uniqueKey[] = $key.':'.$value;
            } else {
                if (is_array($value)) {
                    $uniqueKey[] = $key.':['.self::_createKey($value).']';
                }
            }
        }

        return implode(',', $uniqueKey);
    }

    public static function find($parameters = null)
    {
		// Create an unique key based on the parameters
        $key = self::_createKey($parameters);

        if (!isset(self::$_cache[$key])) {
            // Store the result in the memory cache
            self::$_cache[$key] = parent::find($parameters);
        }

        // Return the result in the cache
        return self::$_cache[$key];
    }

    public static function findFirst($parameters = null)
    {
		// Create an unique key based on the parameters
        $key = self::_createKey($parameters);

        if (!isset(self::$_cache[$key])) {
            // Store the result in the memory cache
            self::$_cache[$key] = parent::findFirst($parameters);
        }

        // Return the result in the cache
        return self::$_cache[$key];
    }
}
