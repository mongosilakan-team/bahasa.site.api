<?php

namespace Base\Models\Custom;

/**
 * ResultWord class.
 */
class ResultWord
{
    public function __construct() {
    }

	public static function found($source, $sourceId, $translationId, $result, $resultId, $resultStatusId, $isOnDictionary, $point, $isAlphabetic, $order, $sourcePosition, $possibleCombination, $wordTypeId, $isHasParticle, $rates, $numberOfRules
    , $sourceRootId, $sourcePrefix, $sourceSuffix
    , $resultRootId // , $resultPrefix, $resultSuffix
    ) {
    	$instance = new self();
		$instance->source = $source;
        $instance->sourceId = $sourceId;
        $instance->translationId = $translationId;
		$instance->result = $result;
		$instance->resultId = $resultId;
		$instance->resultStatusId = $resultStatusId;
		$instance->isOnDictionary = $isOnDictionary;
		$instance->point = $point;
        $instance->isAlphabetic = $isAlphabetic;
		$instance->order = $order;
		$instance->sourcePosition = $sourcePosition;
        $instance->isFound = true;
        $instance->possibleCombination = $possibleCombination;
        $instance->wordTypeId = $wordTypeId;
        $instance->isHasParticle = $isHasParticle;
        $instance->rates = $rates;
        $instance->numberOfRules = $numberOfRules;
        $instance->sourceRootId = $sourceRootId;
        $instance->sourcePrefix = $sourcePrefix;
        $instance->sourceSuffix = $sourceSuffix;
        $instance->resultRootId = $resultRootId;
        // $instance->resultPrefix = $resultPrefix;
        // $instance->resultSuffix = $resultSuffix;
    	return $instance;
    }

	public static function base($source, $isAlphabetic, $order, $sourcePosition) {
    	$instance = new self();
		$instance->source = $source;
		$instance->result = $source;
		$instance->isAlphabetic = $isAlphabetic;
		$instance->order = $order;
        $instance->sourcePosition = $sourcePosition;
		$instance->isFound = false;
    	return $instance;
    }

    public $source;

    public $sourceId;

    public $translationId;

    public $resultId;

    public $result;

    public $resultStatusId;

    public $isOnDictionary;

    public $wordTypeId;

    public $point;

    public $isHasParticle;

    public $prefix;

    // public $prefixType;

    public $suffix;

    // public $suffixType;

    public $isAlphabetic;

	public $order;

    public $sourcePosition = array();

    public $isFound;

    // public $wordRootId;
}
