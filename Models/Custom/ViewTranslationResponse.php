<?php
namespace Base\Models\Custom;

/**
 * ViewTranslationResponse class
 */
class ViewTranslationResponse
{
    public $member = array();

    // Additional field
    public $sourcePrefix;

    // public $prefixType;

    public $sourceSuffix;

    // public $suffixType;

    public $isHasParticle;

    public $isModified;

    public $isCombinationFound = false;
}
