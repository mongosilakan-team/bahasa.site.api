<?php
namespace Base\Models\Custom;

/**
 * ViewTranslationResponse class
 */
class TranslationResult
{
	public $basic;

    public $advanced = array();

}
