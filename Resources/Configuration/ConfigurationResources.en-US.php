<?php

$messages = array(
    "ConfirmPassword" => "Confirm Password",
    "Email" => "Email",
    "email" => "email",
    "Login" => "Login",
    "Password" => "Password",
    "User" => "User",
    "username" => "username",
    "Username" => "Username",
    "UsernameOrEmail" => "Username / Email",
    "Users" => "Users",
    "OauthProvider" => "Oauth Provider",
    "OauthId" => "Oauth Id",
    "ProfilePicture" => "Profile Picture",
);
