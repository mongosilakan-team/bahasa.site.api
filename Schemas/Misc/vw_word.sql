CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `vw_word` AS
    SELECT 
        `w`.`id` AS `id`,
        `w`.`language_id` AS `language_id`,
        `w`.`word` AS `word`,
        `w`.`status_id` AS `status_id`,
        `w`.`is_on_dictionary` AS `is_on_dictionary`,
        `w`.`word_type_id` AS `word_type_id`,
        `w`.`created_at` AS `created_at`,
        `w`.`modified_at` AS `modified_at`,
        `l`.`name` AS `language`,
        `l`.`code` AS `language_code`,
        `wt`.`name` AS `word_type`
    FROM
        ((`word` `w`
        JOIN `language` `l` ON ((`w`.`language_id` = `l`.`id`)))
        JOIN `word_type` `wt` ON ((`wt`.`id` = `w`.`word_type_id`)))