ALTER TABLE  `languages` CHANGE  `id`  `id` TINYINT( 4 ) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `words`
DROP INDEX `language_Id` ;

ALTER TABLE `words`
DROP INDEX `unique_index_combination` ;

ALTER TABLE `users`
DROP FOREIGN KEY `fk_languages_2`;
ALTER TABLE `users`
DROP INDEX `language_id_idx` ;

ALTER TABLE `languages`
CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `users`
CHANGE COLUMN `language_id` `language_id` INT(11) UNSIGNED NULL DEFAULT NULL ;
ALTER TABLE `users`
ADD CONSTRAINT `fk_users_2`
  FOREIGN KEY (`language_id`)
  REFERENCES `languages` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


ALTER TABLE `words`
CHANGE COLUMN `language_id` `language_id` INT(11) UNSIGNED NOT NULL ;

ALTER TABLE `words`
ADD INDEX `fk_wordslang_1_idx` (`language_id` ASC);
ALTER TABLE `words`
ADD CONSTRAINT `fk_wordslang_1`
  FOREIGN KEY (`language_id`)a
  REFERENCES `languages` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

  ALTER TABLE `words`
  ADD UNIQUE INDEX `unique_word_key` (`language_id` ASC, `word` ASC);
