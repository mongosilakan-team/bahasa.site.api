ALTER TABLE `words`
CHANGE COLUMN `word_particle` `prefix` VARCHAR(10) NULL DEFAULT NULL ,
ADD COLUMN `suffix` VARCHAR(10) NULL AFTER `modified_by`,
ADD COLUMN `word_root_id` INT(11) UNSIGNED NULL AFTER `suffix`;

ALTER TABLE `words`
CHANGE COLUMN `word_root_id` `word_root_id` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `word_type_id`,
CHANGE COLUMN `suffix` `suffix` VARCHAR(10) NULL DEFAULT NULL AFTER `prefix`;

CREATE
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `ViewTranslation` AS
    select
        `w`.`word` AS `source`,
        `w`.`id` AS `source_id`,
        `w`.`language_id` AS `source_language_id`,
        `w`.`word_root_id` AS `source_root_id`,
        `w`.`prefix` AS `source_prefix`,
        `w`.`suffix` AS `source_suffix`,
        ((`w`.`prefix` is not null)
            or (`w`.`suffix` is not null)) AS `is_source_has_particle`,
        `j`.`word_root_id` AS `result_root_id`,
        `j`.`prefix` AS `result_prefix`,
        `j`.`suffix` AS `result_suffix`,
        ((`j`.`prefix` is not null)
            or (`j`.`suffix` is not null)) AS `is_result_has_particle`,
        `t`.`translation_id` AS `translation_id`,
        `j`.`id` AS `result_id`,
        `j`.`word` AS `result`,
        `j`.`language_id` AS `result_language_id`,
        `t`.`translation_status_id` AS `translation_status_id`,
        `j`.`is_on_dictionary` AS `is_on_dictionary`,
        `j`.`word_type_id` AS `word_type_id`,
        `t`.`point` AS `point`,
        `t`.`user_id` AS `user_id`,
        `t`.`suggested_by` AS `suggested_by`,
        POSSIBLECOMBINATION(`w`.`id`) AS `possible_combination`,
        GETTRANSLATIONRATES(`t`.`translation_id`) AS `rates`,
        NUMBEROFRULES(`w`.`id`) AS `number_of_rules`
    from
        ((`UnionTranslation` `t`
        join `words` `j` ON (((`j`.`id` = `t`.`first_word_id`)
            or (`j`.`id` = `t`.`second_word_id`))))
        join `words` `w` ON (((`w`.`id` = `t`.`first_word_id`)
            or (`w`.`id` = `t`.`second_word_id`))))
    where
        ((`w`.`id` <> `j`.`id`)
            and (`w`.`language_id` <> `j`.`language_id`))
