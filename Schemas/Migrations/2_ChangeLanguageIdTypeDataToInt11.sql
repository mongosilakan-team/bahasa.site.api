CREATE TABLE `user_trusted_languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_user_trusted_id` (`user_id`,`language_id`),
  KEY `fk_user_trusted_languages_1_idx` (`language_id`),
  KEY `fk_user_trusted_languages_2_idx` (`user_id`),
  CONSTRAINT `fk_user_trusted_languages_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_user_trusted_languages_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
