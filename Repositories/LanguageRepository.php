<?php

namespace Base\Repositories;

use Base\Framework\Constants;
use Base\Models\Language;
use Base\Repositories\Interfaces\ILanguageRepository;


class LanguageRepository extends BaseRepository implements ILanguageRepository
{
    /**
     * Define used model.
     */
    public static function Model()
    {
        return new Language();
    }

    public function getViewModelName()
    {
        return '\Base\Models\Language';
    }

    /**
     *
     */
    public static function getByCode($code)
    {
        return Language::findFirst(array("(code = '$code')"));
    }

    public static function getActive()
    {
		$active = Constants::Status() ['Active'];
        return Language::find(array("(status_id = '$active')"));
    }

    public static function getActiveLanguagesAndByIds($ids)
    {
		$active = Constants::Status() ['Active'];
        $params = $ids ? array("(status_id = '$active' OR id in ($ids))") : array("(status_id = '$active')");
        return Language::find($params);
    }

    public static function getActiveAndPending()
    {
        $active = Constants::Status() ['Active'];
		$pending = Constants::Status() ['Pending'];
        return Language::find(array("(status_id = '$active' OR status_id = '$pending')"));
    }
}
