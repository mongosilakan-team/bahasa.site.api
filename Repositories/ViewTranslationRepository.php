<?php

namespace Base\Repositories;

use Base\Framework\Constants;
use Base\Models\Custom\ViewTranslationResponse;
use Base\Models\ViewTranslation;
use Base\Repositories\Interfaces\IViewTranslationRepository;
use Base\Framework\Plugins\AccessPlugin;
use Base\Framework\DevTools\BaseDebugger;

class ViewTranslationRepository implements IViewTranslationRepository
{
    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Define used model.
     */
    public static function Model()
    {
        return new ViewTranslation();
    }

    public static function getBulkWords($sourceBulk, $fromLanguageId, $toLanguageId)
    {
        $user = AccessPlugin::getCurrentUser();
        $conditions = 'source IN ('.$sourceBulk.') AND source_language_id = :source_language_id: AND result_language_id = :result_language_id: ';
        $conditions .= 'AND (user_id = :user_id: OR user_id = :system_id: OR translation_status_id = :active:)';

        $parameters = array(
            'source_language_id' => $fromLanguageId,
            'result_language_id' => $toLanguageId,
            'active' => Constants::Status() ['Active'],
            'user_id' => $user['id'],
            'system_id' => 2,
        );

        $criteria = array($conditions, 'bind' => $parameters, 'order' => 'rates DESC');

        $result = self::_retrieveByCriteria($criteria);

        return $result;
    }

    /**
     * Check wheter the word is has combination or not. eg. air mata -> luh
     * @param  string  $source                word source
     * @param  int  $fromLanguageId
     * @param  int  $toLanguageId
     * @param  boolean $isCombinationPossible save possibility of combination
     * @return ViewTranslationResponse        Response objects
     */
    // public static function checkCombinationOfWord($source, $fromLanguageId, $toLanguageId, $isCombinationPossible = false)
    // {
    //     $user = AccessPlugin::getCurrentUser();
    //     // Since we can't pass custom field value, we create model mapper
    //     $result = new ViewTranslationResponse();
    //
    //     $conditions = 'source LIKE :source: AND source_language_id = :source_language_id: AND result_language_id = :result_language_id: ';
    //
    //     $conditions .= 'AND (user_id = :user_id: OR user_id = :system_id: OR translation_status_id = :active:)';
    //
    //     $parameters = array(
    //         'source' => $source.'%',
    //         'source_language_id' => $fromLanguageId,
    //         'result_language_id' => $toLanguageId,
    //         'active' => Constants::Status() ['Active'],
    //         'user_id' => $user['id'],
    //         'system_id' => 2,
    //     );
    //
    //     $criteria = array($conditions, 'bind' => $parameters, 'order' => 'rates DESC');
    //
    //     $result = self::_retrieveByCriteria($criteria);
    //     if ($isCombinationPossible) {
    //     // BaseDebugger::debug($source);
    //         $result = self::getSingleWord($source, $fromLanguageId, $toLanguageId);
    //         $result->isCombinationFound = count($result->member) ? true : false;
    //     }
    //     return $result;
    // }

    public static function getSingleWordByWordId($id, $toLanguageId = null)
    {
        // return ViewTranslation::findBySourceId($id);
        if($toLanguageId){
            $user = AccessPlugin::getCurrentUser();
            $conditions = 'source_id = :id: AND (user_id = :user_id: OR user_id = :system_id: OR translation_status_id = :active:) AND source_language_id = :source_language_id: AND result_language_id = :result_language_id:';
            $parameters = array(
                'id' => $id,
                'active' => Constants::Status() ['Active'],
                'source_language_id' => $fromLanguageId,
                'result_language_id' => $toLanguageId,
                'user_id' => $user['id'],
                'system_id' => 2
            );
        }else {
            $conditions = 'source_id = :id: AND translation_status_id = :active:';
            $parameters = array(
                'id' => $id,
                'active' => Constants::Status() ['Active']
            );
        }

        $criteria = array($conditions, 'bind' => $parameters, 'order' => 'rates DESC');

        $result = self::_retrieveByCriteria($criteria);

        return $result;

        // return ViewTranslation::find(array(
        //     "source_id" => $id
        //     //"order" => "rate DESC"
        // ));
    }

    /**
     * Get single word translation from repository
     * @param  string  $source                word source
     * @param  int  $fromLanguageId
     * @param  int  $toLanguageId
     * @return ViewTranslationResponse        Response objects
     */
    public static function getSingleWord($source, $fromLanguageId, $toLanguageId)
    {
        $user = AccessPlugin::getCurrentUser();
        $conditions = 'source = :source: AND source_language_id = :source_language_id: AND result_language_id = :result_language_id: ';
        $conditions .= 'AND (user_id = :user_id: OR user_id = :system_id: OR translation_status_id = :active:)';
        // $conditions .= ' AND (translation_status_id = :active:)';

        // BaseDebugger::debug($conditions);die;
        // BaseDebugger::debug();die;
        $parameters = array('source' => $source,
            'source_language_id' => $fromLanguageId,
            'result_language_id' => $toLanguageId,
            'active' => Constants::Status() ['Active'],
            'user_id' => $user['id'],
            'system_id' => 2,
        );

        $criteria = array($conditions, 'bind' => $parameters, 'order' => 'rates DESC');

        $result = self::_retrieveByCriteria($criteria);

        return $result;
    }

    /**
     * Retrieve from repository by criteria
     * @param  criteria $criteria
     * @return ViewTranslationResponse
     */
    private static function _retrieveByCriteria($criteria)
    {
        try {
            $result = new ViewTranslationResponse();

            $resultSet = ViewTranslation::find($criteria);

            if (count($resultSet)) {
                foreach ($resultSet as $entity) {
                    $member = (object) array();

                    self::_mapper($entity, $member);

                    $member->root = $entity->source;

                    $member->result = $entity->result;

                    $result->member[] = $member;
                }

                $result->word_type_id = $result->member[0]->word_type_id;
            }
            return $result;
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            throw $e;
        }
    }

    /**
     * Mapping object from repository to ViewTranslationResponse
     * @param  object $source model from repository
     * @param  object $target ViewTranslationResponse
     * @return ViewTranslationResponse
     */
    private static function _mapper($source, $target)
    {
        foreach ($source as $k => $v) {
            $target->$k = $v;
        }
    }
}
