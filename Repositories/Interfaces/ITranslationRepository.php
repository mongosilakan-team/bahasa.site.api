<?php
namespace Base\Repositories\Interfaces;

interface ITranslationRepository extends IBaseRepository
{
	public static function checkExistingTranslation($firstWordId, $secondWordId);
}
