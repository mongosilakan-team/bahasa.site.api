<?php
namespace Base\Repositories\Interfaces;

interface IViewTranslationRepository
{
    public static function getSingleWord($source, $fromLangageId, $toLangugeId);
    public static function getSingleWordByWordId($id);
    public static function getBulkWords($source, $fromLanguageId, $toLanguageId);
}
