<?php
namespace Base\Repositories\Interfaces;

interface IBaseTranslationRepository
{
    public static function getSingleWord($criteria);
}