<?php
namespace Base\Repositories;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Base\Framework\Constants;
use Base\Models\Custom\ViewTranslationResponse;
use Base\Models\ViewTranslation;
use Base\Repositories\Interfaces\IViewTranslationRepository;
use Base\Framework\Plugins\AccessPlugin;
use Base\Framework\DevTools\BaseDebugger;

/**
 * ViewTranslation class
 */
class SPViewTranslationRepository extends Model
{
	public static function getBulkWords($source, $fromLanguageId, $toLanguageId)
    {
		try {
			$result = new ViewTranslationResponse();

			$user = AccessPlugin::getCurrentUser();

			$userId = $user['id'];

			$sql = "CALL SPViewTranslation(2, '$source', '$fromLanguageId', '$toLanguageId', '$userId');";
			$robot = new SPViewTranslationRepository();
			$resultSet =  new Resultset(null, $robot, $robot->getReadConnection()->query($sql));
			// BaseDebugger::debug(count($resultSet));die;
            // $resultSet = ViewTranslation::find($criteria);

            if (count($resultSet)) {
                foreach ($resultSet as $entity) {
                    $member = (object) array();

                    self::_mapper($entity, $member);

                    $member->root = $entity->source;

                    $member->result = $entity->result;

                    $result->member[] = $member;
                }

                $result->word_type_id = $result->member[0]->word_type_id;
            }
            return $result;
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            throw $e;
        }
    }

	public static function getSingleWord($source, $fromLanguageId, $toLanguageId)
    {
		$user = AccessPlugin::getCurrentUser();
		$userId = $user['id'];
		$sql = "CALL SPViewTranslation(1, '$source', '$fromLanguageId', '$toLanguageId', '$userId');";
		$robot = new SPViewTranslationRepository();
		return new Resultset(null, $robot, $robot->getReadConnection()->query($sql));
    }

	/**
     * Mapping object from repository to ViewTranslationResponse
     * @param  object $source model from repository
     * @param  object $target ViewTranslationResponse
     * @return ViewTranslationResponse
     */
    private static function _mapper($source, $target)
    {
        foreach ($source as $k => $v) {
            $target->$k = $v;
        }
    }
}
