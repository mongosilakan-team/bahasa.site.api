<?php

namespace Base\Repositories;

use Base\Models\TranslationRates;
use Base\Repositories\Interfaces\ITranslationRatesRepository;

class TranslationRatesRepository extends BaseRepository implements ITranslationRatesRepository
{
    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Define used model.
     */
    public static function Model()
    {
        return new TranslationRates();
    }

    public function getViewModelName()
    {
        return '\Base\Models\TranslationRates';
    }

	public static function checkExistingTranslationRates($fwi, $swi, $userId)
    {
        return TranslationRates::findFirst(array("(((first_word_id = '$swi' OR second_word_id = '$fwi') OR (first_word_id = '$fwi' OR second_word_id = '$swi')) AND suggested_by = '$userId')"));
    }
}
