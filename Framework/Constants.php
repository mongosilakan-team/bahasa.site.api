<?php

namespace Base\Framework;

class Constants
{
    private static $messageTypes = array(
        'Success' => 'success',
        'Warning' => 'warning',
        'Error' => 'error',
        'Info' => 'info',
    );

    public static function getMessageType()
    {
        return self::$messageTypes;
    }

    private static $errorCodes = array(
        'BadRequest' => 400,
        'Unauthorized' => 401,
        'Forbidden' => 403,
        'NotFound' => 404,
        'InternalServerError' => 500,
    );

    public static function errorCode()
    {
        return self::$errorCodes;
    }

    public static $particleCode = array(
        'Prefix' => 'prefix',
        'Suffix' => 'suffix',
    );

    public static function particleCode()
    {
        return self::$particleCode;
    }

    public static $wordTypes = array(
        'Verb' => 1,
        'Adjective' => 2,
        'Pronoun' => 3,
        'Noun' => 4,
        'Adverb' => 5,
        'Conjunction' => 6,
        'Number' => 7,
        'Question' => 8,
    );

    public static function WordTypes($key = null)
    {
        return $key ? self::$wordTypes[$key] : self::$wordTypes;
    }

    private static $particleTypes = array(
        'PrefixType1' => 'PrefixType1',
        'PrefixType2' => 'PrefixType2',
        'PrefixType3' => 'PrefixType3',
        'PrefixType4' => 'PrefixType4',
        'PrefixType5' => 'PrefixType5',
        'PrefixType6' => 'PrefixType6',
        'PrefixType7' => 'PrefixType7',
        'PrefixType8' => 'PrefixType8',
        'PrefixType9' => 'PrefixType9',
        'SuffixType1' => 'SuffixType1',
        'SuffixType2' => 'SuffixType2',
        'SuffixType3' => 'SuffixType3',
        'SuffixType4' => 'SuffixType4',
        'SuffixType5' => 'SuffixType5',
        'SuffixType6' => 'SuffixType6',
        'SuffixType7' => 'SuffixType7',
        'SuffixType8' => 'SuffixType8',
        'SuffixType9' => 'SuffixType9',
        'SuffixType10' => 'SuffixType10',
    );

    public static function ParticleType($id = null)
    {
        return $id ? self::$particleTypes[$id] : self::$particleTypes;
    }

    private static $prefixes = array(
        'id-ID' => array(
            'PrefixType1' => array('di', 'se', 'ku', 'kau', 'menge', 'penge'),
            'PrefixType2' => array('me', 'mem', 'men', 'meny', 'meng'),
            'PrefixType3' => array('pe', 'pen', 'pem', 'peng', 'peny'),
            'PrefixType4' => array('memper'), //case : memperdalam, memperbanyak, mempekerjakan, memperindah
            'PrefixType5' => array('ber'),
            'PrefixType6' => array('ke'),
            'PrefixType7' => array('per'),
            'PrefixType8' => array('ter'),
            'PrefixType9' => array('diper'),
            ),
        'jv-NG' => array(
            'PrefixType1' => array(),
            'PrefixType2' => array(),
            ),
    );

    public static function Prefix($languageId, $type = null)
    {
        if(!isset($prefixes[$languageId])) return array();
        return $type ? self::$prefixes[$languageId][$type] : self::$prefixes[$languageId];
    }

    private static $suffixes = array(
        'id-ID' => array(
            'SuffixType1' => array('kan', 'in', 'kanmu', 'kanku', 'kannya'),
            'SuffixType2' => array('an'),
            'SuffixType3' => array('i', 'imu', 'iku'),
            'SuffixType4' => array('lah'),
            'SuffixType5' => array('mu', 'ku', 'anku', 'anmu'),
            'SuffixType6' => array('nya', 'annya'),
            'SuffixType7' => array('inya'),
            'SuffixType8' => array('ilah', 'ipun'),
            'SuffixType9' => array('pun'),
            'SuffixType10' => array('kanpun'),
            ),
        'jv-NG' => array(
            'SuffixType1' => array(),
            ),
    );

    public static function Suffix($languageId, $type = null)
    {
        if(!isset($suffixes[$languageId])) return array();
        return $type ? self::$suffixes[$languageId][$type] : self::$suffixes[$languageId];
    }

    private static $rules = array(
        'SwitchNext' => 1,
        'SwitchPrevious' => 2,
    );

    public static function Rule($id = null)
    {
        return $id ? self::$rules[$id] : self::$rules;
    }


    private static $statuses = array(
        'Active' => 1,
        'Inactive' => 2,
        'Pending' => 3,
        'Review' => 4,
        'Blocked' => 5,
        'Suggested' => 6
    );

    public static function Status($id = null)
    {
        return $id ? self::$statuses[$id] : self::$statuses;
    }
}
