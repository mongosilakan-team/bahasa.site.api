<?php

namespace Base\Framework\Plugins;

use Phalcon\Mvc\Micro as Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use Base\Framework\DevTools\BaseDebugger;
use Base\Resources\Common\CommonResources;

/**
 * Class Access.
 */
class AccessPlugin implements MiddlewareInterface
{
    public function call(Micro $app)
    {
        // return true;
        switch($app->getRouter()->getRewriteUri()){
            case '/api/user/login':
            case '/api/user/nativeLogin':
            case '/api/user/logout':
            case '/api/resource/':
            case '/api/language/':
            case '/api/language/getActiveAndPending':
            case '/api/language/getUserLanguages':
            case '/api/translate/translate':
            case '/api/word/getSuggestionWordDictionary':
            case '/api/translate/getWordDictionaryDetails':

                return true;
                break;
        }

        if (!$app->session->has('auth')) {
            $this->_unauthorizeAccess();
        }
        // BaseDebugger::debug($app->session->auth['isSuperAdmin']);die;
        if($app->session->auth['isSuperAdmin']){
            return true;
        }

		$route = $app->getRouter()->getRewriteUri();
		$access = $app->session->auth['access'];
		if($this->_validateAccess($route, $access)){
			return true;
		}

	    // All options requests get a 200, then die
	    if($app->__get('request')->getMethod() == 'OPTIONS'){
	        $app->response->setStatusCode(200, 'OK')->sendHeaders();
	        exit;
	    }

		$this->_unauthorizeAccess();
    }

    public static function getCurrentUser(){
        $di = \Phalcon\DI::getDefault();
        $session = $di->getShared('session');
        return $session->get('auth');
    }

	private function _validateAccess($route, $access){
        $access = unserialize($access);
		if(in_array($route, $access)){
			return true;
		}

		if(in_array($route.'/', $access)){
			return true;
		}

		if(is_numeric(end(explode('/', $route)))){
			return true;
		}

		return false;
	}

    private function _unauthorizeAccess()
    {
        throw new \Base\Framework\Exceptions\HTTPException(
            CommonResources::getMessage('UnauthorizedAccess'),
            401,
            array(
                'dev' => 'Please provide credentials by either passing in a session token via cookie, or providing password and username via BASIC authentication.',
                'internalCode' => 'Unauth:1',
            )
        );
    }
}
