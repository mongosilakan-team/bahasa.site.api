<?php

namespace Base\Framework\TranslationKit;

use Base\Framework\Constants;

class TranslationKit
{
    public static function GetParticleType($val, $arr)
    {
        $keyParent = !isset($keyParent) ?: null;

        foreach ($arr as $k => $v) {
            if ($v == $val) {
                return $keyParent;
            }

            if (is_array($v)) {
                $keyParent = $k;

                $result = self::GetParticleType($val, $v);

                if ($result != false) {
                    return $keyParent;
                }
            }
        }

        return false;
    }
}
