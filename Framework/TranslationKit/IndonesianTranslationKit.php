<?php

namespace Base\FrameWork\TranslationKit;

use Base\Framework\Constants;
use Base\Models\Custom\ViewTranslationResponse;
use Base\Repositories\ViewTranslationRepository;


class IndonesianTranslationKit
{
    public static function getTranslation($word, $fromLangageId, $toLanguageId)
    {
        $result = new ViewTranslationResponse();

        $result = ViewTranslationRepository::getSingleWord($word->root, $fromLangageId, $toLanguageId);
        if (!count($result->member)) {
            $possibleInitialChar = array('s', 'm', 'p', 't', 'k', 'e', 'ke', 'n');

            switch ($word->prefixType) {
                  case Constants::ParticleType('PrefixType2'):
                      // case : memakan = mem + akan -> mem + makan
                  case Constants::ParticleType('PrefixType3'):

                      foreach ($possibleInitialChar as $initChar) {
                          $result = ViewTranslationRepository::getSingleWord($initChar.$word->root, $fromLangageId, $toLanguageId);
                          if (count($result->member)) {
                              break;
                          }
                      }
                      break;

                  default:
                      break;
              }
        }

        // $result->wordRootId = null;

        if (isset($result->member[0])) {
            $result->word_type_id = $result->member[0]->word_type_id;
            // $result->wordRootId = $result->member[0]->word_root_id;
            // $result->word_type = $result->member[0]->word_type;
        }

        $result->sourcePrefix = $word->prefix;
        $result->prefixType = $word->prefixType;
        $result->sourceSuffix = $word->suffix;
        $result->suffixType = $word->suffixType;
        // $result->resultRootId = $word->suffixType;
        $result->isModified = true; // the result is modification from system. not pure from database
        $result->isHasParticle = $word->prefix || $word->suffix ? true : false;
        return $result;
    }
}
