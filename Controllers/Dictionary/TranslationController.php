<?php

namespace Base\Controllers\Dictionary;

use Base\Services\TranslationService;
use Phalcon\Http\Request;

/**
 * Translation controller class
 */
class TranslationController extends \Base\Controllers\ApiController
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->translationService = new TranslationService();
        $this->request = new Request();
    }

    /**
     * Get all translation
     * @return json collection of translation
     */
    public function get()
    {
        $response = $this->translationService->getAll();

        return $this->sendRespond($response);
    }

    /**
     * Get one by id
     * @param  int $id identifier
     * @return json     result
     */
    public function getOne($id)
    {
        $response = $this->translationService->getById($id);

        return $this->sendRespond($response);
    }

    /**
     * Create translation
     * @return json result
     */
    public function create()
    {
        $entity = $this->validateEntity($this->request->getPost());

        $response = $this->translationService->create($entity);

        return $this->sendRespond($response);
    }

    /**
     * Update translation
     * @return json result
     */
    public function update()
    {
        $entity = $this->validateEntity($this->request->getPost());

        $response = $this->translationService->update($entity);

        return $response;

        return $this->sendRespond($response);
    }

    /**
     * Delete translation
     * @param  int $id identifier
     * @return json     result
     */
    public function delete($id)
    {
        $response = $this->translationService->delete($id);

        return $this->sendRespond($response);
    }

    /**
     * Search translation
     * @return json result
     */
    public function search()
    {
        $response = $this->translationService->search($this->request->getPost());

        return $response;
    }
}
