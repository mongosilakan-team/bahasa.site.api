<?php

namespace Base\Controllers\Dictionary;

use Base\Services\LanguageService;
use Phalcon\Http\Request;
use Base\Framework\DevTools\BaseDebugger;

/**
 * Language controller class
 */
class LanguageController extends \Base\Controllers\ApiController
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->languageService = new LanguageService();
        $this->request = new Request();
    }

    /**
     * Get all language.
     *
     * @return json collection of language
     */
    public function get()
    {
        $response = $this->languageService->getActive();

        return $this->sendRespond($response);
    }

    public function getActiveAndPending()
    {
        $response = LanguageService::getActiveAndPending();
        // BaseDebugger::debug($response);die;
        return $this->sendRespond($response);
    }

    public function getUserLanguages()
    {
        $response = LanguageService::getUserLanguages();
        // BaseDebugger::debug($response);die;
        return $this->sendRespond($response);
    }

    public function getAll()
    {
        $response = $this->languageService->getAll();

        return $this->sendRespond($response);
    }


    /**
     * Get one by id
     * @param  int $id identifier
     * @return json     language
     */
    public function getOne($id)
    {
        $response = $this->languageService->getById($id);

        return $this->sendRespond($response);
    }

    /**
     * Create language
     * @return json result
     */
    public function create()
    {
        $entity = $this->validateEntity($this->request->getPost());

        $response = $this->languageService->create($entity);

        return $this->sendRespond($response);
    }

    /**
     * Update language
     * @return json result
     */
    public function update()
    {
        $entity = $this->validateEntity($this->request->getPost());

        $response = $this->languageService->update($entity);

        return $this->sendRespond($response);
    }

    /**
     * Delete language
     * @param  int $id identifier
     * @return json     result
     */
    public function delete($id)
    {
        $response = $this->languageService->delete($id);

        return $this->sendRespond($response);
    }

    /**
     * Search language
     * @return json collection of language
     */
    public function search()
    {
        $response = $this->languageService->search($this->request->getPost());

        return $response;
    }
}
