<?php

namespace Base\Controllers\Configurations;

use Base\Services\RoleService;
use Phalcon\Http\Request;

/**
 * Role controller class
 */
class RoleController extends \Base\Controllers\ApiController
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->roleService = new RoleService();
        $this->request = new Request();
    }

    /**
     * Get all role
     * @return json collection of role
     */
    public function get()
    {
        $response = $this->roleService->getAll();

        return $this->sendRespond($response);
    }

    /**
     * Get one by id
     * @param  int $id identifier
     * @return json     role
     */
    public function getOne($id)
    {
        $response = $this->roleService->getById($id);

        return $this->sendRespond($response);
    }

    /**
     * Create role
     * @return json result
     */
    public function create()
    {
        $entity = $this->validateEntity($this->request->getPost());
        $response = $this->roleService->create($entity);

        return $this->sendRespond($response);
    }

    /**
     * Update role
     * @return json result
     */
    public function update()
    {
        $entity = $this->validateEntity($this->request->getPost());
        $response = $this->roleService->update($entity);

        return $this->sendRespond($response);
    }

    /**
     * Delete role
     * @param  int $id identifier
     * @return json     result
     */
    public function delete($id)
    {
        $response = $this->roleService->delete($id);

        return $this->sendRespond($response);
    }

    /**
     * Search role by criteria
     * @return json result
     */
    public function search()
    {
        $response = $this->roleService->search($this->request->getPost());

        return $response;
    }
}
