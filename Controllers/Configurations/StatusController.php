<?php

namespace Base\Controllers\Configurations;

use Base\Services\StatusService;
use Phalcon\Http\Request;

/**
 * Status controller class
 */
class StatusController extends \Base\Controllers\ApiController
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->statusService = new StatusService();
        $this->request = new Request();
    }

    /**
     * Get all status
     * @return json collection of status
     */
    public function get()
    {
        $response = $this->statusService->getAll();

        return $this->sendRespond($response);
    }

    /**
     * Get one by id
     * @param  int $id identifier
     * @return json     status
     */
    public function getOne($id)
    {
        $response = $this->statusService->getById($id);

        return $this->sendRespond($response);
    }

    /**
     * Create status
     * @return json result
     */
    public function create()
    {
        $entity = $this->validateEntity($this->request->getPost());
        $response = $this->statusService->create($entity);

        return $this->sendRespond($response);
    }

    /**
     * Update status
     * @return json result
     */
    public function update()
    {
        $entity = $this->validateEntity($this->request->getPost());
        $response = $this->statusService->update($entity);

        return $this->sendRespond($response);
    }

    /**
     * Delete status
     * @param  int $id identifier
     * @return json     result
     */
    public function delete($id)
    {
        $response = $this->statusService->delete($id);

        return $this->sendRespond($response);
    }

    /**
     * Search status by criteria
     * @return json result
     */
    public function search()
    {
        $response = $this->statusService->search($this->request->getPost());

        return $response;
    }
}
