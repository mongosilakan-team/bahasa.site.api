<?php

namespace Base\Controllers\Configurations;

use Base\Services\UserService;
use Base\Services\RoleService;
use Base\Framework\Constants;
use Base\Resources\Common\CommonResources;
use Phalcon\Http\Request;
use Base\Framework\Messages\Message;
use Phalcon\Http\Response;
use Base\Framework\Library\Auth;
use Base\Framework\Library\String;
use Base\Framework\DevTools\BaseDebugger;

/**
 * User controller class
 */
class UserController extends \Base\Controllers\ApiController
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->userService = new UserService();

        $this->request = new Request();
    }

    /**
     * Get all user
     * @return json collection of user
     */
    public function get()
    {
        $response = $this->userService->getAll();

        return $this->sendRespond($response);
    }

    /**
     * Get one by id
     * @param  int $id identifier
     * @return json     user
     */
    public function getOne($id)
    {
        $response = $this->userService->getById($id);

        return $response;

        return $this->sendRespond($response);
    }

    /**
     * Login
     * @return bool is login success
     */
    public function nativeLogin()
    {
        // return 'asfaf';
        $userOrEmail = $this->request->getPost('username');

        $password = $this->request->getPost('password');

        $response = $this->userService->getByUsernameOrEmail($userOrEmail);
        // return $response;
        if ($response->model) {

            if ($this->security->checkHash($password, $response->model->password)) {

                // $this->_registerSession($response->model);
                $access = $trustedLanguages = array();
                $isSuperAdmin = false;
                foreach ($response->model->roles as $keyRole => $role) {
                    if($role->id == 1){ // Super admin;
                        $isSuperAdmin = true;
                    }
                    foreach ($role->privileges as $key => $privilege) {
                        $access[] = "/api/".$privilege->route."/";
                    }
                }

                foreach ($response->model->trustedLanguages as $key => $lang) {
                        $trustedLanguages[] = $lang->id;
                }

                $response->model->access = $access;
                $response->model->trustedLanguages = $trustedLanguages;
                $response->model->isSuperAdmin = $isSuperAdmin;
                return $response->model;
            }
        }

        return false;
    }

    public function login()
    {

        $response = new Response();

        $ticket = $this->googleClient->verifyIdToken($this->request->getPost('data'));
          if ($ticket) {
            $data = $ticket->getAttributes();
            // BaseDebugger::debug($data['payload']);die;
            $user = $this->userService->getByUsernameOrEmail($data['payload']['email'])->model;
            if(!$user){
                $entity = array();
                $entity['email'] = $data['payload']['email'];
                $entity['username'] = $data['payload']['email'];
                $entity['name'] = $data['payload']['name'];
                $entity['email_verified'] = $data['payload']['email_verified'];
                $entity['oauth_id'] = $data['payload']['sub'];
                $entity['oauth_provider'] = 'google';
                $entity['picture'] = $data['payload']['picture'];
                $entity['locale'] = $data['payload']['locale'];
                $entity['status_id'] = 1;
                $entity['language_id'] = 1;
                $user = $this->userService->create($entity)->model;
                $response->messages[] = new Message(null, Constants::getMessageType() ['Info'], String::format(CommonResources::getMessage('SignUpThanks') , $entity['name']));
            } else {
                $response->messages[] = new Message(null, Constants::getMessageType() ['Info'], String::format(CommonResources::getMessage('SignInThanks') , $user->name));
            }
            $this->_registerSession($user);
            $response->model = (object) array('id'=> $user->id, 'name' =>$user->name, 'picture' => $user->picture);
            return $this->sendRespond($response);
          }

          $response->model = false;
          return $this->sendRespond($response);
    }

    /**
     * Logout
     * @return bool is success
     */
    public function logout()
    {
        $this->session->destroy();

        return true;
    }

    /**
     * Create user
     * @return json result
     */
    public function create()
    {
        $entity = $this->validateEntity($this->request->getPost());

        $entity['password'] = $this->security->hash($entity['password']);

        $response = $this->userService->create($entity);

        return $this->sendRespond($response);
    }

    /**
     * Update user
     * @return json result
     */
    public function update()
    {
        $entity = $this->validateEntity($this->request->getPost());

        $response = $this->userService->update($entity);

        return $this->sendRespond($response);
    }

    /**
     * delete user
     * @param  int $id identifier
     * @return json     result
     */
    public function delete($id)
    {
        $response = $this->userService->delete($id);

        return $this->sendRespond($response);
    }

    /**
     * Search user
     * @return json collection of user
     */
    public function search()
    {
        $response = $this->userService->search($this->request->getPost());

        return $response;
    }

    /**
     * Register session
     * @param  object $user user entity
     */
    private function _registerSession($user)
    {
        $access = $trustedLanguages = array();
        $isSuperAdmin = false;
        foreach ($user->roles as $keyRole => $role) {
            if($role->id == 1){ // Super admin;
                $isSuperAdmin = true;
            }
            foreach ($role->privileges as $key => $privilege) {
                $access[] = "/api/".$privilege->route."/";
            }
        }

        foreach ($user->trustedLanguages as $key => $lang) {
                $trustedLanguages[] = $lang->id;
        }

        $this->session->set('auth', array('id' => $user->id, 'name' => $user->name, 'isSuperAdmin'=> $isSuperAdmin, 'access'=> serialize($access),'trustedLanguages'=> serialize($trustedLanguages),'picture'=>$user->picture));
    }
}
